## Interface: 11302
## Author: cqwrteur
## Title: MusicBox Options
## Version: @project-version@
## Dependencies: MusicBox
## LoadOnDemand: 1

#@no-lib-strip@
Libs\AceDBOptions-3.0\AceDBOptions-3.0.xml
Libs\AceLocale-3.0\AceLocale-3.0.xml
#@end-no-lib-strip@

Init.lua
locale\locale.xml
module\module.xml
Core.lua