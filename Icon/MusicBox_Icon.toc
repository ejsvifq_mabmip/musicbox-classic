## Interface: 11302
## Author: cqwrteur
## Title: MusicBox Icon
## Version: @project-version@
## Dependencies: MusicBox
## SavedVariablesPerCharacter: MusicBox_IconCharacterDB

#@no-lib-strip@
Libs\LibDataBroker-1.1\LibDataBroker-1.1.lua
Libs\LibDBIcon-1.0\lib.xml
#@end-no-lib-strip@

Core.lua